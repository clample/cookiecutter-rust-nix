# Cookiecutter Rust+Nix

Creates a [cookiecutter](https://cookiecutter.readthedocs.io) template for a Rust project.

The project uses:

* [nix](https://nixos.org)
* [direnv](https://direnv.net/)
* [lorri](https://github.com/target/lorri)

