{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  RUST_BACKTRACE=1;
  buildInputs = [
    pkgs.cargo
    pkgs.rustc
    pkgs.rustfmt
    pkgs.rust-analyzer
  ];
  
}

